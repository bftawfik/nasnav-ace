import React from 'react';

import aceLogo from '../../../assets/img/ace-logo.png';

import * as styles from './Logo.module.css';

export default props => (
  <img src={aceLogo} alt="" className={styles.Logo}></img>
)